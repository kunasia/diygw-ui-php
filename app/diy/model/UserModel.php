<?php
// +----------------------------------------------------------------------
// | Diygw PHP
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2023 https://www.diygw.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: diygw <diygwcom@diygw.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\diy\model;

use diygw\FileUtil;
use diygw\model\DiygwModel;

/**
 * @mixin \diygw\model\DiygwModel
 * @package app\diy\model
 */
class UserModel extends DiygwModel
{
    // 表名
    public $name = 'diy_user';

    // 相似查询字段
    protected $likeField=[];

    public function setBaseToImg($base64_image_content){
        //匹配出图片的格式
        $preg = preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result);
        if ($preg){
            $type = $result[2];
            $new_file = str_replace('\\', '/',  \config('filesystem.disks.local.root'). DIRECTORY_SEPARATOR .'avatar'.DIRECTORY_SEPARATOR);
            if(!file_exists($new_file))
            {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                FileUtil::mk_dirs($new_file);
            }
            $new_file = $new_file.uniqid().".{$type}";
            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
                return  $url = $this->getLocalPath($new_file);;
            }else{
                return false;
            }
        }else{
            return $base64_image_content;
        }
    }

    public function beforeEdit(&$data){
        if(isset($data['avatar'])&&!empty($data['avatar'])){
            $data['avatar'] = $this->setBaseToImg($data['avatar']);
        }
        return true;
    }

    /**
     * 本地路径
     * @param $path
     * @return string
     */
    protected function getLocalPath($filePath): string
    {
        $path = str_replace(str_replace('\\', '/', root_path('public')),  '',str_replace('\\', '/',  $filePath)) ;
        return app()->request->domain() .'/'.  $path;
    }

}
